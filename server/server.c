//
// Created by ifelseelif on 03.09.2021.
//

#include "server.h"

int server_status = ACTIVE;
int client_act_status = INACTIVE;


void execute_command(Common__Request *request, Common__Response *response) {
    response->text = "ok";
    switch (request->command_code) {
        case COMMON__COMMAND_CODE__CREATE:
            command_create(request, response);
            break;
        case COMMON__COMMAND_CODE__DELETE:
            command_delete(request, response);
            break;
        case COMMON__COMMAND_CODE__MATCH:
            command_match(request, response);
            break;
        case COMMON__COMMAND_CODE__SET:
            command_set(request, response);
            break;
        default:
            break;
    }
}

void serve_client(int socket) {
    printf("Serve thread for socket %d created\n", socket);
    do {
        printf("--------------WAIT NEW REQUEST FROM CLIENT: %d\n", socket);
        Common__Request *request;
        receive_request(socket, &request);

        if (request == NULL) {
            fprintf(stderr, "%s\n", ERROR_MESSAGE_UNPACK);
            client_act_status = INACTIVE;
            printf("deactivate client socket: %d\n", socket);
            close(socket);
            return;
        }

        Common__Response response = COMMON__RESPONSE__INIT;
        execute_command(request, &response);

        int response_stat = send_response(socket, &response);
        if (response_stat == 1) break;


//        for (int i = 0; i < response.n_nodes; i++) {
//            free(response.nodes[i]);
//        }
//        common__response__free_unpacked(&response, NULL);
    } while (client_act_status == ACTIVE);
    close(socket);
}

void send_connect(int socket) {
    printf("Prepare check connection packet\n");

    Common__Response response = COMMON__RESPONSE__INIT;

    response.command_code = COMMAND_CODE_CONNECT;
    response.status_code = STATUS_OK;
    response.text = "Connected";

    unsigned long len = common__response__get_packed_size(&response);
    printf("Packet packed size: %lu\n", len);

    void *buf = malloc(len);
    common__response__pack(&response, buf);
    printf("COMMAND CODE: %d\n", response.command_code);

    send_header(socket, len);
    write(socket, buf, len);
    printf("Sent check connection packet\n");
    free(buf);
}


int run_server(int port) {
    if (init() == -1) return -1;
    struct sockaddr_in server_address, client_address;
    int server_socket;
    int reuse = 1;

    long return_code = init_server_connection(&server_address, port, MAX_CLIENTS_AMOUNT, &server_socket, &reuse);
    if (return_code != SOCKET_CODE_OK) return return_code;

    int client_sock;
    while (server_status == ACTIVE) {
        client_sock = accept(server_socket, NULL, NULL);
        if (client_sock == -1) {
            if (errno == FNONBLOCK) {
                printf("No pending connections; sleeping for one second.\n");
                sleep(1);
            } else {
                perror("error when accepting connection\n");
                exit(1);
            }
        }
        printf("-----------NEW CONNECTION ACCEPTED; Client Socket: %d\n", client_sock);
        send_connect(client_sock);
        client_act_status = ACTIVE;
        printf("Connection response sent\n");
        serve_client(client_sock);
    }
    if (client_sock < 0) {
        perror("accept failed");
        exit(1);
    }
    return 0;
}
