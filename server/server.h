//
// Created by ifelseelif on 03.09.2021.
//

#ifndef SPO_LAB_1_5_SERVER_H
#define SPO_LAB_1_5_SERVER_H

#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <netinet/in.h>

#include "utility/commands_executer.h"
#include "../commons/net/socket-net.h"


#define MAX_MSG_SIZE 1024

#define ERROR_MESSAGE_UNPACK "unpacking failed"
#define MAX_CLIENTS_AMOUNT 30

int run_server(int port);

#endif //SPO_LAB_1_5_SERVER_H
