//
// Created by ifelseelif on 11.09.2021.
//

#ifndef SPO_LAB_1_5_COMMANDS_EXECUTER_H
#define SPO_LAB_1_5_COMMANDS_EXECUTER_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "../../commons/net/protocol/message.pb-c.h"
#include "../../graph_module/graph_struct.h"
#include "../../commons/collections/linked_list.h"

void command_create(Common__Request *request, Common__Response *response);

void command_delete(Common__Request *request, Common__Response *response);

void command_match(Common__Request *request, Common__Response *response);

void command_set(Common__Request *request, Common__Response *response);

#endif //SPO_LAB_1_5_COMMANDS_EXECUTER_H
