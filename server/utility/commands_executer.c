//
// Created by ifelseelif on 11.09.2021.
//

#include "commands_executer.h"

void fill_node_response_props(Common__Node *response_node, long id_node) {
    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);

    get_block(block, (id_node * OFFSET_FULL_BLOCK) + OFFSET_TYPE, OFFSET_BLOCK);
    int count = 0;
    long current_id_labels = block->node.id_props;
    linked_list *keys = init_list();
    linked_list *values = init_list();
    while (current_id_labels != 0 && count < 10) {
        count++;
        get_block(block, current_id_labels * OFFSET_FULL_BLOCK + OFFSET_TYPE, OFFSET_BLOCK);
        add_last(keys, block->node_Props.key);
        add_last(values, block->node_Props.value);
        current_id_labels = block->node_Props.id_next_node_props;
    }

    node *current_key = keys->first;
    node *current_value = values->first;
    response_node->props = malloc(sizeof(Common__Prop) * keys->size);
    response_node->n_props = keys->size;
    for (int q = 0; q < keys->size; q++) {
        Common__Prop *response_prop = malloc(sizeof(Common__Prop));
        common__prop__init(response_prop);
        response_node->props[q] = response_prop;

        long len_key = strlen(current_key->value);
        long len_value = strlen(current_value->value);

        response_node->props[q]->key = malloc(len_key);
        response_node->props[q]->key_len = len_key;

        response_node->props[q]->value = malloc(len_value);
        response_node->props[q]->value_len = len_value;

        strncpy(response_node->props[q]->key, current_key->value, len_key);
        strncpy(response_node->props[q]->value, current_value->value, len_value);

        current_key = current_key->next;
        current_value = current_value->next;
    }

    free_list(keys, false);
    free_list(values, false);
    free(block);
}

void fill_node_response_labels(Common__Node *response_node, long id_node) {
    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);

    get_block(block, (id_node * OFFSET_FULL_BLOCK) + OFFSET_TYPE, OFFSET_BLOCK);
    int count = 0;
    long current_id_labels = block->node.id_labels;
    linked_list *labels = init_list();
    while (current_id_labels != 0 && count < 10) {
        count++;
        get_block(block, current_id_labels * OFFSET_FULL_BLOCK + OFFSET_TYPE, OFFSET_BLOCK);
        for (int j = 0; j < block->node_Labels.n_labels; j++) {
            add_last(labels, block->node_Labels.labels[j]);
        }
        current_id_labels = block->node_Labels.id_next_node_labels;
    }

    node *current_label = labels->first;
    response_node->labels = malloc(sizeof(Common__Label) * labels->size);
    response_node->n_labels = labels->size;
    for (int q = 0; q < labels->size; q++) {
        Common__Label *response_label = malloc(sizeof(Common__Label));
        common__label__init(response_label);
        response_node->labels[q] = response_label;

        long label_len = strlen(current_label->value);

        response_node->labels[q]->value = malloc(label_len);
        response_node->labels[q]->value_len = label_len;

        strncpy(response_node->labels[q]->value, current_label->value, label_len);
        current_label = current_label->next;
    }
    free_list(labels, false);
    free(block);
}

void build_response(Common__Response *response, linked_list *list, int list_size) {
    response->nodes = malloc(sizeof(Common__Node) * list_size);
    response->n_nodes = list_size;
    node *current_id_node = list->first;

    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);

    for (int i = 0; i < list_size; i++) {
        Common__Node *response_node = malloc(sizeof(Common__Node));
        common__node__init(response_node);
        response->nodes[i] = response_node;

        fill_node_response_labels(response->nodes[i], (long) current_id_node->value);
        fill_node_response_props(response->nodes[i], (long) current_id_node->value);

        current_id_node = current_id_node->next;
    }

    free(block);
}

void add_labels_props_to_db(Common__Node *node_in, union Block *node_out) {
    long prop_id = 0;
    if (node_in->n_props > 0) {
        for (int i = 0; i < node_in->n_props; i++) {
            union Block nodeProps;
            memcpy(nodeProps.node_Props.key, node_in->props[i]->key, PROPERTY_FIELD_SIZE);
            memcpy(nodeProps.node_Props.value, node_in->props[i]->value, PROPERTY_FIELD_SIZE);
            nodeProps.node_Props.id_next_node_props = prop_id;

            prop_id = add_block(&nodeProps, NODE_PROPS_CODE);
        }
    }
    node_out->node.id_props = prop_id;

    long labels_id = 0;
    if (node_in->n_labels > 0) {
        for (int i = 0; i < node_in->n_labels; i++) {
            union Block nodeLabels;
            memcpy(nodeLabels.node_Labels.labels[i % SIZE_LIST_LABELS_NODE], node_in->labels[i]->value,
                   SIZE_LABEL_STRING);

            if ((i % SIZE_LIST_LABELS_NODE == 0 && i != 0) || node_in->n_labels == 1) {
                nodeLabels.node_Labels.id_next_node_labels = labels_id;
                if (node_in->n_labels == 1) {
                    nodeLabels.node_Labels.n_labels = 1;
                } else {
                    nodeLabels.node_Labels.n_labels = i % SIZE_LIST_LABELS_NODE;
                }
                labels_id = add_block(&nodeLabels, NODE_LABELS_CODE);
            }
        }
    }
    node_out->node.id_relationship = 0;
    node_out->node.id_labels = labels_id;
}

void create_relationships(Common__Node *from_node, Common__Node *to_node, char *relation_name) {
    linked_list *from_node_ids = get_all_nodes_with_labels_props(from_node);
    linked_list *to_node_ids = get_all_nodes_with_labels_props(to_node);

    if (from_node_ids->size == 0 || to_node_ids->size == 0) {
        return;
    }

    node *current_from_node_id = from_node_ids->first;
    for (int i = 0; i < from_node_ids->size; i++) {

        node *current_to_node_id = to_node_ids->first;
        for (int j = 0; j < to_node_ids->size; j++) {
            union Block block;

            long rel_id = find_id_for_rel((long) current_from_node_id->value);
            block.node_Relation.id = rel_id;
            block.node_Relation.id_next_node_sibling = 0;
            block.node_Relation.id_node_from = (long) current_from_node_id->value;
            block.node_Relation.id_node_to = (long) current_to_node_id->value;
            strcpy(block.node_Relation.name, relation_name);

            printfBlock();
            add_block_with_id(&block, NODE_RELATION_CODE, rel_id);

            printfBlock();
            current_to_node_id = current_to_node_id->next;
        }
        current_from_node_id = current_from_node_id->next;
    }
}

void find_nodes_with_path_length(linked_list *from_nodes_id, Common__Response *response, int32_t path_length,
                                 int32_t limit) {
    if (path_length == 0) {
        int list_size;
        if (limit > from_nodes_id->size) {
            list_size = from_nodes_id->size;
        } else {
            list_size = limit;
        }
        build_response(response, from_nodes_id, list_size);
        return;
    }
    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);
    long offset;

    long rel_id;
    linked_list *result = init_list();
    node *current_from_node_id = from_nodes_id->first;
    for (int i = 0; i < from_nodes_id->size; i++) {
        offset = (OFFSET_FULL_BLOCK * (long) current_from_node_id->value) + OFFSET_TYPE;
        get_block(block, offset, OFFSET_BLOCK);
        rel_id = block->node.id_relationship;
        while (rel_id != 0) {
            offset = (OFFSET_FULL_BLOCK * rel_id) + OFFSET_TYPE;
            get_block(block, offset, OFFSET_BLOCK);

            add_last(result, (void *) block->node_Relation.id_node_to);
            rel_id = block->node_Relation.id_next_node_sibling;
        }

        current_from_node_id = current_from_node_id->next;
    }
    free_list(from_nodes_id, false);
    path_length--;
    find_nodes_with_path_length(result, response, path_length, limit);
}

void command_create(Common__Request *request, Common__Response *response) {
    long last_id = 0;
    union Block node;

    if (request->connection != NULL && request->return_type == COMMON__RETURN_TYPE__RELATION) {
        create_relationships(request->node, request->connection->connected_node, request->connection->relation);
        return;
    }

    add_labels_props_to_db(request->node, &node);
    long nodeId = add_block(&node, NODE_CODE);

    if (request->connection != NULL) {
        union Block connectedNode;
        add_labels_props_to_db(request->connection->connected_node, &connectedNode);
        last_id = add_block(&connectedNode, NODE_CODE);

        union Block nodeRelationship;
        nodeRelationship.node_Relation.id_node_to = last_id;
        nodeRelationship.node_Relation.id_node_from = nodeId;
        nodeRelationship.node_Relation.id_next_node_sibling = 0;
        memcpy(nodeRelationship.node_Relation.name, request->connection->relation, RELATION_NAME_SIZE);
        long id_relationship = add_block(&nodeRelationship, NODE_RELATION_CODE);
        node.node.id_relationship = id_relationship;
        update(nodeId, NODE_CODE, &node);
    }

    printfBlock();
}

void command_set(Common__Request *request, Common__Response *response) {
    linked_list *list_of_id_node = get_all_nodes_with_labels_props(request->node);
    node *current_id_node = list_of_id_node->first;

    union Block node;

    if (request->updated->n_props > 0) {
        for (int i = 0; i < request->updated->n_props; i++) {
            union Block nodeProps;
            memcpy(nodeProps.node_Props.key, request->updated->props[i]->key, PROPERTY_FIELD_SIZE);
            memcpy(nodeProps.node_Props.value, request->updated->props[i]->value, PROPERTY_FIELD_SIZE);
            nodeProps.node_Props.id = (long) current_id_node->value - 1 - (long) request->updated->n_props + i;
            if (i == 0) {
                nodeProps.node_Props.id_next_node_props = 0;
            } else {
                nodeProps.node_Props.id_next_node_props = nodeProps.node_Props.id;
            }

            update((long) current_id_node->value - 2, NODE_PROPS_CODE, &nodeProps);
        }
    }

}

void command_delete(Common__Request *request, Common__Response *response) {
    linked_list *list_result;
    linked_list *list_of_id_node = get_all_nodes_with_labels_props(request->node);
    if (request->connection == NULL) {
        list_result = list_of_id_node;
        delete_blocks(list_result);
    } else {
        linked_list *list_of_id_connected_node = get_all_nodes_with_labels_props(request->connection->connected_node);
        linked_list *list_of_id_connections = get_all_connections(list_of_id_node, list_of_id_connected_node);
        switch (request->return_type) {
            case COMMON__RETURN_TYPE__NODE:
                list_result = intersection(list_of_id_node, list_of_id_connections, true);
                break;
            case COMMON__RETURN_TYPE__RELATION:
                list_result = list_of_id_connections;
                break;
            case COMMON__RETURN_TYPE__CONNECTED_NODE:
                list_result = intersection(list_of_id_connected_node, list_of_id_connections, false);
                break;
        }

        delete_blocks(list_result);
        free_list(list_of_id_connected_node, false);
        free_list(list_of_id_connections, false);
    }

    free_list(list_of_id_node, false);
    response->text = "DELETE success";
}

void command_match(Common__Request *request, Common__Response *response) {
    response->text = "MATCH";
    response->status_code = 1;

    if (request->pathlength > 0) {
        linked_list *from_nodes_id = get_all_nodes_with_labels_props(request->node);
        find_nodes_with_path_length(from_nodes_id, response, request->pathlength, request->limit);
        return;
    }

    linked_list *list_result;
    linked_list *list_of_nodes_ids = get_all_nodes_with_labels_props(request->node);
    int list_size;
    if (request->limit > list_of_nodes_ids->size) {
        list_size = list_of_nodes_ids->size;
    } else {
        list_size = request->limit;
    }

    if (request->connection == NULL) {
        list_result = list_of_nodes_ids;
        build_response(response, list_result, list_size);
    } else {
        linked_list *list_of_id_connected_node = get_all_nodes_with_labels_props(request->connection->connected_node);
        linked_list *list_of_id_connections = get_all_connections(list_of_nodes_ids, list_of_id_connected_node);
        switch (request->return_type) {
            case COMMON__RETURN_TYPE__NODE:
                list_result = intersection(list_of_nodes_ids, list_of_id_connections, true);
                break;
            case COMMON__RETURN_TYPE__RELATION:
                list_result = list_of_id_connections;
                break;
            case COMMON__RETURN_TYPE__CONNECTED_NODE:
                list_result = intersection(list_of_id_connected_node, list_of_id_connections, false);
                break;
        }

        build_response(response, list_result, list_size);
        free_list(list_of_id_connected_node, false);
        free_list(list_of_id_connections, false);
    }

    free_list(list_of_nodes_ids, false);
}
