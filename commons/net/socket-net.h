//
// Created by ifelseelif on 04.09.2021.
//

#ifndef SPO_LAB_1_5_SOCKET_NET_H
#define SPO_LAB_1_5_SOCKET_NET_H

#include <stdio.h>
#include <stdint.h>
#include "stdbool.h"
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#include "protocol/message.pb-c.h"
#include <sys/socket.h>


long init_server_connection(struct sockaddr_in *server_address, long socket_port, int max_clients_amount,
                            int *connect_socket, int *reuse);

int socket_open(int *connect_socket);

int socket_connect(const int *client_socket, struct sockaddr_in *server_address);

void socket_close(const int *socket);

int socket_set_name(const int *connect_socket, int *reuse);

int socket_check_connect(const int *socket);

int socket_bind(const int *connect_socket, struct sockaddr_in *server_address);

int socket_listen(const int *connect_socket, int max_clients_amount);

int socket_nonblock(const int connect_socket);

bool validate_ip(char *address);

bool validate_port(int port);

void read_buffer(int socket, void *out, unsigned max_length);

void write_buffer(int socket, void *in, unsigned length);

unsigned long receive_header(int socket);

int send_header(int socket, unsigned long len);

int send_request(int socket, Common__Request *request);

int send_response(int socket, Common__Response *request);

int receive_request(int socket, Common__Request **request);

int receive_response(int socket, Common__Response **response);


#define PRINTLN printf("\n")

#define MESSAGE_INFO_SOCKET_OPEN "Open socket..."
#define MESSAGE_INFO_OK "OK"
#define MESSAGE_INFO_SOCKET_CONNECT "Connect socket..."


#define MESSAGE_SOCKET_CHECK "Check connection..."
#define MESSAGE_CLIENTS_FULL "Amount of client is full"
#define MESSAGE_SOCKET_LISTEN "Listen socket..."
#define MESSAGE_INFO_SOCKET_SET_NAME "Set socket label..."
#define MESSAGE_INFO_SOCKET_BIND "Bind socket..."
#define MESSAGE_SOCKET_CLOSE "Close socket"
#define MESSAGE_INIT_FINISH "Init finished"

#define MESSAGE_ERROR_SOCKET_OPEN "Open socket failed"
#define MESSAGE_ERROR_SOCKET_CONNECT "Connect socket failed"
#define MESSAGE_ERROR_CLIENTS_OVERFLOW "Amount of clients is overflowed"
#define MESSAGE_ERROR_SOCKET_ACCEPT "Socket accept() failed"
#define MESSAGE_ERROR_CONNECT_NEW_CLIENT "New client cant be connected"
#define MESSAGE_ERROR_SEND "Send error"
#define MESSAGE_ERROR_SOCKET_LISTEN "Listen socket failed"
#define MESSAGE_ERROR_SOCKET_SET_NAME "Set socket label failed"
#define MESSAGE_ERROR_SOCKET_BIND "Bind socket failed"

#define SOCKET_CODE_OK 0
#define SOCKET_CODE_ERROR_OPEN 4
#define SOCKET_CODE_ERROR_SOCKET_ACCEPT 3
#define SOCKET_CODE_ERROR_SOCKET_SET_NAME 5
#define SOCKET_CODE_ERROR_SOCKET_BIND  6
#define SOCKET_CODE_ERROR_SOCKET_LISTEN 7
#define SOCKET_CODE_ERROR_CONNECT 9

#define STATUS_OK 50
#define STATUS_ERROR 51

#define COMMAND_CODE_CONNECT 20
#define COMMAND_CODE_SELECT 21
#define COMMAND_CODE_CREATE 22
#define COMMAND_CODE_DELETE 23
#define COMMAND_CODE_UPDATE 24

#define MAX_MSG_SIZE 1024


#define ACTIVE 1
#define INACTIVE 0

#endif //SPO_LAB_1_5_SOCKET_NET_H
