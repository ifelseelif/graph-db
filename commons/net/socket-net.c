//
// Created by ifelseelif on 04.09.2021.
//

#include "socket-net.h"

int socket_open(int *connect_socket) {
    printf(MESSAGE_INFO_SOCKET_OPEN);
    *connect_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (connect_socket < 0) {
        perror(MESSAGE_ERROR_SOCKET_OPEN);
        PRINTLN;
        return SOCKET_CODE_ERROR_OPEN;
    } else {
        printf(MESSAGE_INFO_OK);
        PRINTLN;
        return SOCKET_CODE_OK;
    }
}

int socket_connect(const int *client_socket, struct sockaddr_in *server_address) {
    printf(MESSAGE_INFO_SOCKET_CONNECT);
    if (connect(*client_socket, (struct sockaddr *) server_address, sizeof(struct sockaddr_in)) == -1) {
        perror(MESSAGE_ERROR_SOCKET_CONNECT);
        PRINTLN;
        return SOCKET_CODE_ERROR_CONNECT;
    } else {
        printf(MESSAGE_INFO_OK);
        PRINTLN;
        return SOCKET_CODE_OK;
    }
}

int socket_set_name(const int *connect_socket, int *reuse) {
    printf(MESSAGE_INFO_SOCKET_SET_NAME);
    if (setsockopt(*connect_socket, SOL_SOCKET, SO_REUSEADDR, (const char *) reuse, sizeof(int)) == -1) {
        perror(MESSAGE_ERROR_SOCKET_SET_NAME);
        PRINTLN;
        return SOCKET_CODE_ERROR_SOCKET_SET_NAME;
    } else {
        printf(MESSAGE_INFO_OK);
        PRINTLN;
        return SOCKET_CODE_OK;
    }
}

int socket_bind(const int *connect_socket, struct sockaddr_in *server_address) {
    printf(MESSAGE_INFO_SOCKET_BIND);
    if (bind(*connect_socket, (struct sockaddr *) server_address, sizeof(struct sockaddr_in)) == -1) {
        perror(MESSAGE_ERROR_SOCKET_BIND);
        PRINTLN;
        return SOCKET_CODE_ERROR_SOCKET_BIND;
    } else {
        printf(MESSAGE_INFO_OK);
        PRINTLN;
        return SOCKET_CODE_OK;
    }
}

int socket_check_connect(const int *socket) {
    printf(MESSAGE_SOCKET_CHECK);
    Common__Response *response;
    uint8_t buf[BUFSIZ];
    unsigned long response_size = receive_header(*socket);
    size_t msg_len = 0;
    msg_len = read(*socket, buf, response_size);
    printf("Recieved response size: %zu\n", msg_len);

    response = common__response__unpack(NULL, msg_len, buf);
    if (response == NULL) {
        fprintf(stderr, "error unpacking incoming message\n");
        return 1;
    }
    printf("Status code: %d\n", response->status_code);
    printf("Recieved text: %s\n", response->text);
    if (response->status_code != STATUS_OK) {
        close(*socket);
        printf("Wrong command\n");
        return SOCKET_CODE_ERROR_CONNECT;
    }
    common__response__free_unpacked(response, NULL);
    printf(MESSAGE_INFO_OK);
    return 0;
}

int send_header(int socket, unsigned long len) {
    char buffer[BUFSIZ];
    memset(buffer, 0, BUFSIZ);
    snprintf(buffer, BUFSIZ, "%lu", len);
    write(socket, buffer, BUFSIZ);
    return 0;
}

unsigned long receive_header(int socket) {
    unsigned long response_size;
    char header[BUFSIZ];
    read(socket, header, BUFSIZ);
    response_size = strtol(header, NULL, 10);
    return response_size;
}

int receive_request(int socket, Common__Request **request) {
    uint8_t buf[BUFSIZ];
    size_t msg_len = receive_header(socket);
    msg_len = read(socket, buf, msg_len);
    if (msg_len < 1) {
        printf("Cannot read message\n");
        return 1;
    }
    *request = common__request__unpack(NULL, msg_len, buf);
    if (*request == NULL) return 1;
    return 0;
}

int send_request(int socket, Common__Request *request) {
    size_t len_send = common__request__get_packed_size(request);
    void *buf_send = malloc(len_send);
    common__request__pack(request, buf_send);

    send_header(socket, len_send);
    if (write(socket, buf_send, len_send) == -1) {
        fprintf(stderr, MESSAGE_ERROR_SEND);
        return 1;
    };
    return 0;
}

int send_response(int socket, Common__Response *response) {
    if (response->status_code == 1) {
        send_header(socket, 0);
        size_t len_send = common__response__get_packed_size(response);
        send_header(socket, len_send);
        void *buf_send = malloc(len_send);
        common__response__pack(response, buf_send);
        write_buffer(socket, buf_send, len_send);
        free(buf_send);
    } else {
        size_t len_send = common__response__get_packed_size(response);
        void *buf_send = malloc(len_send);
        unsigned long len = common__response__pack(response, buf_send);
        send_header(socket, len_send);
        if (write(socket, buf_send, len_send) < 0) return 1;
    }
    return 0;
}

int receive_response(int socket, Common__Response **response) {
    uint8_t buf[BUFSIZ];
    size_t msg_len = receive_header(socket);
    if (msg_len == 0) {
        size_t size = receive_header(socket);
        void *buf1 = malloc(size);
        read_buffer(socket, buf1, size);
        *response = common__response__unpack(NULL, size, buf1);
        free(buf1);
    } else {
        if (msg_len < 1) return 1;
        msg_len = read(socket, buf, msg_len);
        *response = common__response__unpack(NULL, msg_len, buf);
    }
    return 0;
}


int socket_listen(const int *connect_socket, int max_clients_amount) {
    printf(MESSAGE_SOCKET_LISTEN);
    if (listen(*connect_socket, max_clients_amount) == -1) {
        printf(MESSAGE_ERROR_SOCKET_LISTEN);
        perror(MESSAGE_ERROR_SOCKET_LISTEN);
        PRINTLN;
        return SOCKET_CODE_ERROR_SOCKET_LISTEN;
    }
    printf(MESSAGE_INFO_OK);
    PRINTLN;
    return 0;
}

int socket_nonblock(const int connect_socket) {
    int flags = fcntl(connect_socket, F_GETFL);
    fcntl(connect_socket, F_SETFL, flags | O_NONBLOCK);
}


long init_server_connection(struct sockaddr_in *server_address, long socket_port, int max_clients_amount,
                            int *connect_socket, int *reuse) {
    int return_code = SOCKET_CODE_OK;

    return_code = socket_open(connect_socket);
    if (return_code != 0) return return_code;

    server_address->sin_family = AF_INET;
    server_address->sin_port = htons(socket_port);
    server_address->sin_addr.s_addr = htonl(INADDR_ANY);

    return_code = socket_set_name(connect_socket, reuse);
    if (return_code != 0) return return_code;
    return_code = socket_bind(connect_socket, server_address);
    if (return_code != 0) return return_code;
    return_code = socket_listen(connect_socket, max_clients_amount);
    if (return_code != 0) return return_code;
    printf(MESSAGE_INIT_FINISH);

    PRINTLN;

    return SOCKET_CODE_OK;
}

bool validate_ip(char *address) {
    struct sockaddr_in sa;
    return inet_pton(AF_INET, address, &(sa.sin_addr)) != 0;
}

bool validate_port(int port) {
    return port < 65535 && port > 1;
}

void read_buffer(int socket, void *out, unsigned content_length) {
    long response_offset = 0;
    long remain_data = content_length;
    void *buf = malloc(BUFSIZ);
    while (remain_data > 0) {
        bzero(buf, BUFSIZ);
        long receive_len = recv(socket, buf, BUFSIZ, 0);
        memcpy(out + response_offset, buf, receive_len);
        remain_data -= receive_len;
        response_offset += receive_len;
    }
}

void write_buffer(int socket, void *in, unsigned length) {
    long remain_data = length;
    int offset = 0;
    do {
        int packet_size = remain_data > BUFSIZ ? BUFSIZ : (int) remain_data;
        char *data = malloc(packet_size);
        memcpy(data, in + offset, packet_size);
        offset += packet_size;

        if (send(socket, data, packet_size, 0) < 0) return;
        free(data);

        remain_data -= packet_size;
    } while (remain_data > 0);
}
