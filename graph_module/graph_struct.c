//
// Created by ifelseelif on 17.06.2021.
//

#include "graph_struct.h"

long last_id;
linked_list *list_of_free_node;
linked_list *list_of_id_node;

int open_storage_file(FILE **file) {
    *file = fopen(FILE_NAME, "r+b");
    return 1;
}

void write_block(void *value, long size, long offset) {
    FILE *file;
    open_storage_file(&file);
    fseek(file, offset, SEEK_SET);
    fwrite(value, size, 1, file);
    printf("Write table bytes to: %ld size: %ld\n", offset, size);
    fclose(file);
}

void fill_zone_by_zero(FILE *file, long size, long offset) {
    void *zeros = malloc(size);
    memset(zeros, 0, size);
    fseek(file, offset, SEEK_SET);
    fwrite(zeros, size, 1, file);
    free(zeros);
}

void fill_list_of_empty_nodes(struct stat st) {
    printf("Size of data file : %ld bytes\n", st.st_size);
    long block_size = st.st_size / OFFSET_FULL_BLOCK;
    long offset;
    long type;

    FILE *data_file;
    open_storage_file(&data_file);

    for (long i = 1; i < block_size; i++) {
        offset = i * OFFSET_FULL_BLOCK;
        fseek(data_file, offset, SEEK_SET);
        fread(&type, OFFSET_TYPE, 1, data_file);
        if (type == 0) {
            add_last(list_of_free_node, (void *) i);
        }
        if (type == NODE_CODE) {
            add_last(list_of_id_node, (void *) i);
        }
    }

    fclose(data_file);
    printfBlock();
    if (block_size == 0) {
        last_id = 1;
    } else {
        last_id = block_size;
    }
    printf("All nodes in file : %ld\n", last_id - 1);
    printf("Empty nodes in file : %d\n\n", list_of_free_node->size);
}

long find_space_for_block() {
    long block_id;
    if (list_of_free_node->size == 0) {
        block_id = last_id;
        last_id++;
    } else {
        long id = (long) list_of_free_node->first->value;
        remove_first(list_of_free_node);
        if (id == -1) {
            block_id = last_id;
            last_id++;
        } else {
            block_id = id;
        }
    }

    return block_id;
}

void delete(long id) {
    FILE *data_file;
    open_storage_file(&data_file);
    long offset = id * OFFSET_FULL_BLOCK;
    fill_zone_by_zero(data_file, OFFSET_TYPE, offset);
    fill_zone_by_zero(data_file, OFFSET_BLOCK, offset + OFFSET_TYPE);
    fclose(data_file);
    add_last(list_of_free_node, (void *) id);
}

void delete_block(long id) {
    if (id == 0) return;

    long type;
    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);

    long offset = id * OFFSET_FULL_BLOCK;
    get_block(&type, offset, OFFSET_TYPE);
    get_block(block, offset + OFFSET_TYPE, OFFSET_BLOCK);

    switch (type) {
        case NODE_CODE:
            delete_block(block->node.id_relationship);
            delete_block(block->node.id_labels);
            delete_block(block->node.id_props);
            break;
        case NODE_PROPS_CODE:
            delete_block(block->node_Props.id_next_node_props);
            break;
        case NODE_LABELS_CODE:
            delete_block(block->node_Labels.id_next_node_labels);
            break;
        case NODE_RELATION_CODE:
            delete_block(block->node_Relation.id_next_node_sibling);
            break;
        case 0:
            return;
    }

    delete(id);
    free(block);
}

int init() {
    struct stat buffer;
    if (stat(FILE_NAME, &buffer) != 0) {
        printf("File not found");
        return -1;
    }
    list_of_id_node = init_list();
    list_of_free_node = init_list();
    fill_list_of_empty_nodes(buffer);

    printf("File successfully found\n");
    return 0;
}

long add_block(union Block *block, long type) {
    long block_id = find_space_for_block();
    switch (type) {
        case NODE_PROPS_CODE:
            block->node_Props.id = block_id;
            break;
        case NODE_RELATION_CODE:
            block->node_Relation.id = block_id;
            break;
        case NODE_LABELS_CODE:
            block->node_Labels.id = block_id;
            break;
        default:
            add_last(list_of_id_node, (void *) block_id);
            block->node.id = block_id;
            break;
    }
    add_block_with_id(block, type, block_id);
    return block_id;
}

long add_block_with_id(union Block *block, long type, long id) {
    long offset = OFFSET_FULL_BLOCK * id;

    write_block(&type, OFFSET_TYPE, offset);
    write_block(block, OFFSET_BLOCK, offset + OFFSET_TYPE);
    return id;
}


long find_id_for_rel(long block_id) {
    if (block_id == 0) {
        return 0;
    }

    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);

    long type;
    long result;

    get_block(block, block_id * OFFSET_FULL_BLOCK + OFFSET_TYPE, OFFSET_BLOCK);
    get_block(&type, block_id * OFFSET_FULL_BLOCK, OFFSET_TYPE);

    if (type == NODE_CODE) {
        result = find_id_for_rel(block->node.id_relationship);
        if (result == 0) {
            result = find_space_for_block();
            block->node.id_relationship = result;
            update(block_id, type, block);
            free(block);
            return result;
        }
    } else {
        if (block->node_Relation.id_next_node_sibling == 0) {
            result = find_space_for_block();
            block->node_Relation.id_next_node_sibling = result;
            update(block_id, type, block);
        } else {
            result = find_id_for_rel(block->node_Relation.id_next_node_sibling);
        }
    }
    free(block);
    return result;
}

void update(long id, long type, union Block *block) {
    long offset = OFFSET_FULL_BLOCK * id;
    long blockType;

    FILE *data_file;
    open_storage_file(&data_file);
    fseek(data_file, offset, SEEK_SET);
    fread(&blockType, OFFSET_TYPE, 1, data_file);
    fclose(data_file);

    if (blockType != type) {
        return;
    }
    write_block(block, OFFSET_BLOCK, offset + OFFSET_TYPE);
}

void delete_blocks(linked_list *list) {
    node *current_id = list->first;
    for (int i = 0; i < list->size; i++) {
        delete_block((long) current_id->value);
        current_id = current_id->next;
    }
}

linked_list *get_all_nodes_with_labels_props(Common__Node *in_node) {
    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);
    long type;

    node *current_id_node = list_of_id_node->first;
    linked_list *result = init_list();
    for (int i = 0; i < list_of_id_node->size; i++) {
        long offset = (long) current_id_node->value * OFFSET_FULL_BLOCK;
        get_block(&type, offset, OFFSET_TYPE);
        get_block(block, offset + OFFSET_TYPE, OFFSET_BLOCK);
        if (type != NODE_CODE) {
            return result;
        }
        long next_id_block = block->node.id_labels;
        int has_needed_labels = 0;
        while (next_id_block != 0) {
            long label_offset = next_id_block * OFFSET_FULL_BLOCK;
            get_block(&type, label_offset, OFFSET_TYPE);
            get_block(block, label_offset + OFFSET_TYPE, OFFSET_BLOCK);
            if (type != NODE_LABELS_CODE) return result;
            for (int j = 0; j < SIZE_LIST_LABELS_NODE; j++) {
                for (int j1 = 0; j1 < in_node->n_labels; j1++) {
                    if (strcmp(block->node_Labels.labels[j], in_node->labels[j1]->value) == 0) {
                        has_needed_labels++;
                    }
                }
            }
            next_id_block = block->node_Labels.id_next_node_labels;
        }
        offset = (long) current_id_node->value * OFFSET_FULL_BLOCK;
        get_block(block, offset + OFFSET_TYPE, OFFSET_BLOCK);
        next_id_block = block->node.id_props;
        while (next_id_block != 0) {
            long props_offset = next_id_block * OFFSET_FULL_BLOCK;
            get_block(&type, props_offset, OFFSET_TYPE);
            get_block(block, props_offset + OFFSET_TYPE, OFFSET_BLOCK);
            if (type != NODE_PROPS_CODE) {
                return result;
            }
            for (int j1 = 0; j1 < in_node->n_props; j1++) {
                if (strcmp(block->node_Props.key, in_node->props[j1]->key) == 0 &&
                    strcmp(block->node_Props.value, in_node->props[j1]->value) == 0) {
                    has_needed_labels++;
                }
            }
            next_id_block = block->node_Labels.id_next_node_labels;
        }

        if (in_node->n_labels + in_node->n_props == has_needed_labels) {
            add_last(result, current_id_node->value);
        }

        current_id_node = current_id_node->next;
    }

    free(block);
    return result;
}

linked_list *get_all_connections(linked_list *from, linked_list *to) {
    FILE *data_file;
    open_storage_file(&data_file);

    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);
    long type;

    linked_list *result = init_list();
    node *current_label = from->first;
    for (int i = 0; i < from->size; i++) {
        long offset = (long) current_label->value * OFFSET_FULL_BLOCK;
        fseek(data_file, offset, SEEK_SET);
        fread(&type, OFFSET_TYPE, 1, data_file);
        fseek(data_file, offset + OFFSET_TYPE, SEEK_SET);
        fread(block, OFFSET_BLOCK, 1, data_file);

        offset = block->node.id_relationship * OFFSET_FULL_BLOCK;
        fseek(data_file, offset + OFFSET_TYPE, SEEK_SET);
        fread(block, OFFSET_BLOCK, 1, data_file);


        node *node_to = from->first;
        for (int i1 = 0; i1 < to->size; i1++) {
            if (block->node_Relation.id_node_to == (long) node_to->value) {
                add_last(result, (void *) block->node_Relation.id);
            }
            node_to = node_to->next;
        }

        current_label = current_label->next;
    }

    free(block);
    fclose(data_file);

    return result;
}

linked_list *intersection(linked_list *id_nodes, linked_list *connections_id, bool is_left) {
    union Block *block = malloc(OFFSET_BLOCK);
    memset(block, 0, OFFSET_BLOCK);

    linked_list *result = init_list();
    node *current_connect_id = connections_id->first;
    long id_node;
    for (int i = 0; i < connections_id->size; i++) {
        get_block(block, ((long) current_connect_id->value * OFFSET_FULL_BLOCK) + OFFSET_TYPE, OFFSET_BLOCK);

        if (is_left == true) {
            id_node = (long) find_element(by_value, id_nodes, (char *) block->node_Relation.id_node_to, NULL);
        } else {
            id_node = (long) find_element(by_value, id_nodes, (char *) block->node_Relation.id_node_from, NULL);
        }
        if (id_node != 0) {
            add_last(result, (void *) id_node);
        }
    }

    return result;
}

void get_block(void *block, long offset, long size) {
    FILE *data_file;
    open_storage_file(&data_file);
    fseek(data_file, offset, SEEK_SET);
    fread(block, size, 1, data_file);;
    fclose(data_file);
}

void printfBlock() {
    union Block *node = malloc(OFFSET_BLOCK);
    memset(node, 0, OFFSET_BLOCK);
    long type;

    struct stat st;
    stat(FILE_NAME, &st);
    long block_size = st.st_size / OFFSET_FULL_BLOCK;

    printf("\nPrint all elements\n");

    FILE *data_file;
    open_storage_file(&data_file);
    for (long i = 1; i < block_size; i++) {
        long offset = i * OFFSET_FULL_BLOCK;
        fseek(data_file, offset, SEEK_SET);
        fread(&type, OFFSET_TYPE, 1, data_file);
        fseek(data_file, offset + OFFSET_TYPE, SEEK_SET);
        fread(node, OFFSET_BLOCK, 1, data_file);
        switch (type) {
            case 1:
                printf("Props :id=%lu next_id=%lu key=%s value=%s \n", node->node_Props.id,
                       node->node_Props.id_next_node_props, node->node_Props.key,
                       node->node_Props.value);
                break;
            case 2:
                printf("Relation :id=%lu next_id=%lu name=%s id_from=%lu id_to=%lu\n", node->node_Relation.id,
                       node->node_Relation.id_next_node_sibling, node->node_Relation.name,
                       node->node_Relation.id_node_from, node->node_Relation.id_node_to);
                break;
            case 3:

                printf("Labels :id=%ld next_id=%ld label1=%s\n",
                       node->node_Labels.id,
                       node->node_Labels.id_next_node_labels,
                       node->node_Labels.labels[0] == NULL ? "" : node->node_Labels.labels[0]);
                break;
            default:
                printf("Node :id=%ld relation_id=%ld labels_id=%ld props_id=%ld\n", node->node.id,
                       node->node.id_relationship, node->node.id_labels,
                       node->node.id_props);
                break;
        }
    }
    fclose(data_file);
    free(node);
}
