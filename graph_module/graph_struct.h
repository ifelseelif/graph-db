//
// Created by ifelseelif on 16.06.2021.
//

#ifndef SPO_LAB_1_5_GRAPH_STRUCT_H
#define SPO_LAB_1_5_GRAPH_STRUCT_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

#include "../commons/collections/linked_list.h"
#include "../commons/net/protocol/message.pb-c.h"

#define FILE_NAME "./disk.txt"

#define NODE_PROPS_CODE 1
#define NODE_RELATION_CODE 2
#define NODE_LABELS_CODE 3
#define NODE_CODE 4

// OFFSET_BLOCK * id_node = адрес структуры. Все структуры должны быть не больше определенного размера.
// OFFSET_BLOCK - это размер блока, куда кладется структура.
// block = преамбула + структура = 64 байт
// преамбула - long, которая служит, чтобы определить как структура хранится
// структура - см. ниже.
#define OFFSET_BLOCK 56

#define OFFSET_FULL_BLOCK 64

#define OFFSET_TYPE 8

// Основная структура, которая хранит id_node на данные и на отношения
// id_props             - id блока, где хранятся наши атрибуты или данные
// id_relationship     - id блока, где хранится описание связей с другими нодами(struct Relation)
// sizeof(Node) == 56.
typedef struct Node {
    char empty_space[24];
    long id;
    long id_props;
    long id_labels;
    long id_relationship;
} Node;

#define SIZE_LIST_LABELS_NODE 4
#define SIZE_LABEL_STRING 8

//sizeof(Node_Labels) = 56;
typedef struct Node_Labels {
    long id;
    long id_next_node_labels;
    long n_labels;
    //label <= 8 bytes
    char labels[SIZE_LIST_LABELS_NODE][SIZE_LABEL_STRING];
} Node_Labels;

#define PROPERTY_FIELD_SIZE 20

// Структура для хранения атрибутов.
// id_next_node_props  - id_node следующего блока
// props              - кусок данных. Должен быть меньше или равен 40.
// sizeof(Node_Props) + sizeof(*data) = 16 + 40 = 56
typedef struct Node_Props {
    long id;
    long id_next_node_props;

    //*data <= 40 bytes
    // TODO продумать хранение атрибутов.
    char key[PROPERTY_FIELD_SIZE];
    char value[PROPERTY_FIELD_SIZE];
} Node_Props;

#define RELATION_NAME_SIZE 24

// Структура для связей.
// id_next_node_sibling  - id_node следующего блока
// sizeof(Node_Relation) = 56
typedef struct Node_Relation {
    long id;
    char name[RELATION_NAME_SIZE];
    long id_node_from;
    long id_node_to;
    long id_next_node_sibling;
} Node_Relation;

union Block {
    struct Node node;
    struct Node_Props node_Props;
    struct Node_Relation node_Relation;
    struct Node_Labels node_Labels;
};

int init();

long add_block(union Block *block, long type);

long add_block_with_id(union Block *block, long type, long id);

long find_id_for_rel(long block_id);

void update(long id, long type, union Block *block);

void delete(long id);

void get_block(void *block, long offset, long size);

linked_list *get_all_nodes_with_labels_props(Common__Node *node);

linked_list *get_all_connections(linked_list *from, linked_list *to);

linked_list *intersection(linked_list *to_id_nodes, linked_list *connections_id, bool is_left);

void delete_blocks(linked_list *list);

void printfBlock();

#endif //SPO_LAB_1_5_GRAPH_STRUCT_H