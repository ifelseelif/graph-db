#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "generator.h"

#include "../graph_module/graph_struct.h"
#include "stdio.h"

#define ALPHABET_SIZE 10

int generate_nodes(long count) {

    srand(time(NULL));

    init();

    char buf[20];
    char buf_char[2];

    char alphabet[ALPHABET_SIZE] = "1234567890";

    for (long i = 0; i < count; i++) {

        long last_id = 0;
        union Block node;

        union Block nodeProps;

        memset(buf, 0, PROPERTY_FIELD_SIZE);
        snprintf(buf, PROPERTY_FIELD_SIZE, "K");
        for (int j = 0; j < 18; j++) {
            buf_char[0] = alphabet[random() % 10];
            buf_char[1]='\0';
            strcat(buf, buf_char);
        }

        memcpy(nodeProps.node_Props.key, buf, PROPERTY_FIELD_SIZE);

        memset(buf, 0, PROPERTY_FIELD_SIZE);
        snprintf(buf, PROPERTY_FIELD_SIZE, "V");
        for (int j = 0; j < 18; j++) {
            buf_char[0] = alphabet[random() % 10];
            buf_char[1]='\0';
            strcat(buf, buf_char);
        }

        memcpy(nodeProps.node_Props.value, buf, PROPERTY_FIELD_SIZE);
        nodeProps.node_Props.id_next_node_props = last_id;

        last_id = add_block(&nodeProps, NODE_PROPS_CODE);

        node.node.id_props = last_id;

        last_id = 0;

        union Block nodeLabels;
        memset(buf, 0, PROPERTY_FIELD_SIZE);
        snprintf(buf, SIZE_LABEL_STRING, "L");
        for (int j = 0; j < 6; j++) {
            buf_char[0] = alphabet[random() % 10];
//            buf_char[1]='\0';
            strcat(buf, buf_char);
        }

        memcpy(nodeLabels.node_Labels.labels[0], buf, SIZE_LABEL_STRING);
        nodeLabels.node_Labels.id_next_node_labels = last_id;
        nodeLabels.node_Labels.n_labels = 1;

        last_id = add_block(&nodeLabels, NODE_LABELS_CODE);

        node.node.id_labels = last_id;
        long nodeId = add_block(&node, NODE_CODE);

    }
    return 0;
}