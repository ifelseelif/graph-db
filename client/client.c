//
// Created by ifelseelif on 06.09.2021.
//

#include "client.h"

int server_socket;
int client_status = ACTIVE;

void print_response(Common__Response *response) {
    printf("\n%s\t", response->text);
    if (response->n_nodes > 0) {
        printf("nodes : %zu \n", response->n_nodes);
        for (int i = 0; i < response->n_nodes; i++) {
            printf("{:");
            for (int j = 0; j < response->nodes[i]->n_labels; j++) {
                for (int q = 0; q < response->nodes[i]->labels[j]->value_len; q++) {
                    printf("%c", response->nodes[i]->labels[j]->value[q]);
                }
                if (j != response->nodes[i]->n_labels - 1) {
                    printf(",");
                }
            }
            printf("{");


            for (int j = 0; j < response->nodes[i]->n_props; j++) {
                for (int q = 0; q < response->nodes[i]->props[j]->key_len; q++) {
                    printf("%c", response->nodes[i]->props[j]->key[q]);
                }
                printf(" : ");

                for (int q = 0; q < response->nodes[i]->props[j]->value_len; q++) {
                    printf("%c", response->nodes[i]->props[j]->value[q]);
                }

                if (j != response->nodes[i]->n_props - 1) {
                    printf(", ");
                }
            }
            printf("}}\n");
        }
    }
}

int connect_server(char *ip, long port, int *socket) {
    int return_code = SOCKET_CODE_OK;
    return_code = socket_open(socket);
    if (return_code != SOCKET_CODE_OK) return return_code;

    struct sockaddr_in server_address = {.sin_family = AF_INET, .sin_port = htons(port)};
    struct in_addr serv_address;
    if (inet_pton(AF_INET, ip, &serv_address) != 0) {
        server_address.sin_addr = serv_address;
    } else {
        server_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    }

    return_code = socket_connect(socket, &server_address);
    if (return_code != SOCKET_CODE_OK) return return_code;

    return return_code;
}

int run_client(char *address, long port) {
    signal(SIGINT, exit);

    int return_code = connect_server(address, port, &server_socket);
    if (return_code > 0) return return_code;

    if (server_socket < 0) {
        printf("Wrong server socket\n");
        return 1;
    }

    int rc = socket_check_connect(&server_socket);
    if (rc != SOCKET_CODE_OK) exit(rc);

    uint8_t buf[BUFSIZ];

    char *input;
    size_t len;

    while (client_status == ACTIVE) {
        printf(PROMT);
        getdelim(&input, &len, '\n', stdin);

        Common__Request request = COMMON__REQUEST__INIT;
        int parsed_value = parse_input_cypher(input, &request);
        if (parsed_value != SOCKET_CODE_OK) continue;
        int return_send = send_request(server_socket, &request);
        if (return_send != 0) continue;

        printf("Message sent\n");

        Common__Response *response;
        return_send = receive_response(server_socket, &response);
        if (return_send != 0) continue;
        print_response(response);
        common__response__free_unpacked(response, NULL);
    }
    return SOCKET_CODE_OK;
}
