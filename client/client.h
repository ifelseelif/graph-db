//
// Created by ifelseelif on 06.09.2021.
//

#ifndef SPO_LAB_1_5_CLIENT_H
#define SPO_LAB_1_5_CLIENT_H

#include <cypher-parser.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>

#include "../commons/net/protocol/message.pb-c.h"
#include "utility/cypher_parser.h"

int run_client(char *address, long port);

#define PROMT "\n\nPrint your message on cypher : "

#endif //SPO_LAB_1_5_CLIENT_H
