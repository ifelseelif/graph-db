//
// Created by ifelseelif on 10.09.2021.
//

#ifndef SPO_LAB_1_5_CYPHER_PARSER_H
#define SPO_LAB_1_5_CYPHER_PARSER_H

#include <malloc.h>
#include <cypher-parser.h>

#include "../../commons/net/socket-net.h"
#include "../../graph_module/graph_struct.h"
#include "../../commons/net/protocol/message.pb-c.h"

enum type_return_enum {
    return_node,
    return_relation,
    return_connected_node
};

typedef struct query_info {
    int command_type;
    linked_list *labels;
    linked_list *props;
    bool has_relation;
    char rel_name[RELATION_NAME_SIZE];
    int path_length;
    linked_list *rel_node_labels;
    linked_list *rel_node_props;
    linked_list *changed_labels;
    linked_list *changed_props;
    Common__ReturnType type_return;
    int limit;
} query_info;

typedef struct property {
    char key[PROPERTY_FIELD_SIZE];
    char value[PROPERTY_FIELD_SIZE];
} property;

int parse_input_cypher(const char *input, Common__Request *pRequest);

#endif //SPO_LAB_1_5_CYPHER_PARSER_H