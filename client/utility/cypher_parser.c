//
// Created by ifelseelif on 10.09.2021.
//

#include "cypher_parser.h"

query_info *init_query_info() {
    query_info *info = malloc(sizeof(query_info));
    info->labels = init_list();
    info->props = init_list();
    info->rel_node_labels = init_list();
    info->rel_node_props = init_list();
    info->changed_labels = init_list();
    info->changed_props = init_list();
    info->has_relation = false;
    info->type_return = COMMON__RETURN_TYPE__NODE;
    return info;
}


void set_labels_and_props(const cypher_astnode_t *node, linked_list *labels, linked_list *properties) {
    for (int i = 0; i < cypher_ast_node_pattern_nlabels(node); ++i) {
        const cypher_astnode_t *label = cypher_ast_node_pattern_get_label(node, i);
        add_last(labels, (void *) cypher_ast_label_get_name(label));
    }
    const cypher_astnode_t *props = cypher_ast_node_pattern_get_properties(node);
    if (props != NULL) {
        for (int i = 0; i < cypher_ast_map_nentries(props); ++i) {
            const cypher_astnode_t *key = cypher_ast_map_get_key(props, i);
            const cypher_astnode_t *value = cypher_ast_map_get_value(props, i);
            property *prop = malloc(sizeof(property));
            memset(prop, 0, PROPERTY_FIELD_SIZE + PROPERTY_FIELD_SIZE);
            if (
                    strlen(cypher_ast_prop_name_get_value(key)) > PROPERTY_FIELD_SIZE ||
                    cypher_astnode_type(value) != CYPHER_AST_STRING ||
                    strlen(cypher_ast_string_get_value(value)) > PROPERTY_FIELD_SIZE
                    ) {
                printf("Invalid Property. Max key size = %d. Max value size = %d. Only string properties allowed.\n",
                       PROPERTY_FIELD_SIZE, PROPERTY_FIELD_SIZE);
            } else {
                strcpy(prop->key, cypher_ast_prop_name_get_value(key));
                strcpy(prop->value, cypher_ast_string_get_value(value));
                add_last(properties, prop);
            }
        }
    }
}

void set_changed_labels_and_props(const cypher_astnode_t *clause, query_info *info) {
    if (cypher_astnode_type(clause) == CYPHER_AST_SET) {
        const cypher_astnode_t *item = cypher_ast_set_get_item(clause, 0);
        if (cypher_astnode_type(item) == CYPHER_AST_SET_LABELS) {
            for (int i = 0; i < cypher_ast_set_labels_nlabels(item); ++i) {
                const cypher_astnode_t *set_label = cypher_ast_set_labels_get_label(item, i);
                add_last(info->changed_labels, (void *) cypher_ast_label_get_name(set_label));
            }
        }
        if (cypher_astnode_type(item) == CYPHER_AST_SET_PROPERTY) {
            const cypher_astnode_t *set_prop = cypher_ast_set_property_get_property(item);
            const cypher_astnode_t *prop_name = cypher_ast_property_operator_get_prop_name(set_prop);
            const cypher_astnode_t *expression = cypher_ast_set_property_get_expression(item);
            property *prop = malloc(sizeof(property));
            if (
                    strlen(cypher_ast_prop_name_get_value(prop_name)) >
                    PROPERTY_FIELD_SIZE ||
                    cypher_astnode_type(expression) != CYPHER_AST_STRING ||
                    strlen(cypher_ast_string_get_value(expression)) > PROPERTY_FIELD_SIZE
                    ) {
                printf("Invalid Property. Only string properties allowed.\n");
            } else {
                strcpy(prop->key, cypher_ast_prop_name_get_value(prop_name));
                strcpy(prop->value, cypher_ast_string_get_value(expression));
                add_last(info->changed_props, prop);
            }
        }
    }
    if (cypher_astnode_type(clause) == CYPHER_AST_REMOVE) {
        const cypher_astnode_t *item = cypher_ast_remove_get_item(clause, 0);
        if (cypher_astnode_type(item) == CYPHER_AST_REMOVE_LABELS) {
            for (int i = 0; i < cypher_ast_remove_labels_nlabels(item); ++i) {
                const cypher_astnode_t *set_label = cypher_ast_remove_labels_get_label(item, i);
                add_last(info->changed_labels, (void *) cypher_ast_label_get_name(set_label));
            }
        }
        if (cypher_astnode_type(item) == CYPHER_AST_REMOVE_PROPERTY) {
            const cypher_astnode_t *remove_prop = cypher_ast_remove_property_get_property(item);
            const cypher_astnode_t *prop_name = cypher_ast_property_operator_get_prop_name(remove_prop);
            property *prop = malloc(sizeof(property));
            if (
                    strlen(cypher_ast_prop_name_get_value(prop_name)) >
                    PROPERTY_FIELD_SIZE) {
                printf("Invalid Property. Max key size = %d\n",
                       PROPERTY_FIELD_SIZE);
            } else {
                strcpy(prop->key, cypher_ast_prop_name_get_value(prop_name));
                strcpy(prop->value, "");
                add_last(info->changed_props, prop);
            }
        }
    }
}

query_info *get_query_info(cypher_parse_result_t *result) {
    query_info *query_info = init_query_info();
    const cypher_astnode_t *ast = cypher_parse_result_get_directive(result, 0);
    const cypher_astnode_t *query = cypher_ast_statement_get_body(ast);
    const cypher_astnode_t *clause = cypher_ast_query_get_clause(query, 0);
    const cypher_astnode_t *clause_return = cypher_ast_query_get_clause(query, 1);
    const cypher_astnode_t *limit = cypher_ast_return_get_limit(clause_return);

    if (limit != NULL) {
        query_info->limit = (int) strtol(cypher_ast_integer_get_valuestr(limit), NULL, 10);
    } else {
        query_info->limit = -1;
    }

    cypher_astnode_type_t command = cypher_astnode_type(clause);
    if (command == CYPHER_AST_MATCH) {
        const cypher_astnode_t *clause1 = cypher_ast_query_get_clause(query, 1);
        if (clause1 == NULL) {
            puts("Unknown command");
            return NULL;
        }
        command = cypher_astnode_type(clause1);
        if (command == CYPHER_AST_RETURN) {
            query_info->command_type = COMMON__COMMAND_CODE__MATCH;
        }
        if (command == CYPHER_AST_SET) {
            query_info->command_type = COMMON__COMMAND_CODE__SET;
        }
        if (command == CYPHER_AST_REMOVE) {
            query_info->command_type = COMMON__COMMAND_CODE__REMOVE;
        }
        if (command == CYPHER_AST_DELETE) {
            query_info->command_type = COMMON__COMMAND_CODE__DELETE;
        }
        set_changed_labels_and_props(clause1, query_info);
    }
    if (command == CYPHER_AST_CREATE) {
        query_info->command_type = COMMON__COMMAND_CODE__CREATE;
    }
    cypher_astnode_t *pattern;
    if (cypher_astnode_type(clause) == CYPHER_AST_MATCH) {
        pattern = cypher_ast_match_get_pattern(clause);
    } else {
        pattern = cypher_ast_create_get_pattern(clause);
    }
    const cypher_astnode_t *path = cypher_ast_pattern_get_path(pattern, 0);
    const cypher_astnode_t *node = cypher_ast_pattern_path_get_element(path, 0);
    set_labels_and_props(node, query_info->labels, query_info->props);
    if (cypher_ast_pattern_path_nelements(path) == 3) {
        query_info->has_relation = true;
        const cypher_astnode_t *relation = cypher_ast_pattern_path_get_element(path, 1);
        const cypher_astnode_t *map = cypher_ast_rel_pattern_get_varlength(relation);
        if (map != NULL) {
            char *res = cypher_ast_integer_get_valuestr(cypher_ast_range_get_start(map));
            query_info->path_length = (int) strtol(res, NULL, 10);
        } else {
            query_info->path_length = 0;
        }

        const cypher_astnode_t *identifier_relation = cypher_ast_rel_pattern_get_identifier(relation);
        if (identifier_relation != NULL) query_info->type_return = COMMON__RETURN_TYPE__RELATION;
        const cypher_astnode_t *rel_type = cypher_ast_rel_pattern_get_reltype(relation, 0);
        if (cypher_ast_reltype_get_name(rel_type) != NULL) {
            memcpy(query_info->rel_name, cypher_ast_reltype_get_name(rel_type), RELATION_NAME_SIZE);
        }
        const cypher_astnode_t *rel_node = cypher_ast_pattern_path_get_element(path, 2);
        const cypher_astnode_t *identifier_connected_node = cypher_ast_node_pattern_get_identifier(rel_node);
        if (identifier_connected_node != NULL) query_info->type_return = COMMON__RETURN_TYPE__CONNECTED_NODE;
        set_labels_and_props(rel_node, query_info->rel_node_labels, query_info->rel_node_props);
    }
    return query_info;
}

void fill_labels(Common__Node *node_in, linked_list *list) {
    node_in->labels = malloc(sizeof(Common__Label) * list->size);

    node *current_label = list->first;
    node_in->n_labels = list->size;
    for (int i = 0; i < list->size; i++) {
        Common__Label *response_label = malloc(sizeof(Common__Label));
        common__label__init(response_label);
        char *buf = current_label->value;
        node_in->labels[i] = response_label;
        node_in->labels[i]->value = buf;
        node_in->labels[i]->value_len = strlen(buf);
        current_label = current_label->next;
    }
};

void fill_props(Common__Node *node_in, linked_list *list) {
    node_in->props = malloc(sizeof(Common__Prop) * list->size);

    node *current_prop = list->first;
    node_in->n_props = list->size;
    for (int i = 0; i < list->size; i++) {
        Common__Prop *prop = malloc(sizeof(Common__Prop));
        common__prop__init(prop);
        property *current = current_prop->value;
        prop->key = current->key;
        prop->key_len = strlen(current->key);

        prop->value = current->value;
        prop->value_len = strlen(current->value);

        node_in->props[i] = prop;
        current_prop = current_prop->next;
    }
}

char *build_client_request(query_info *info, Common__Request *request) {
    request->command_code = info->command_type;
    Common__Node *node = malloc(sizeof(Common__Node));
    common__node__init(node);
    fill_labels(node, info->labels);
    fill_props(node, info->props);
    request->node = node;

    request->limit = info->limit;
    request->pathlength = info->path_length;


    if (info->has_relation == true) {
        Common__Connection *connection = malloc(sizeof(Common__Connection));
        common__connection__init(connection);
        connection->relation = info->rel_name;

        Common__Node *connected_node = malloc(sizeof(Common__Node));
        common__node__init(connected_node);

        fill_labels(connected_node, info->rel_node_labels);
        fill_props(connected_node, info->rel_node_props);
        request->connection = connection;
        request->connection->connected_node = connected_node;
    }
    if (info->command_type == COMMON__COMMAND_CODE__SET || info->command_type == COMMON__COMMAND_CODE__REMOVE) {
        Common__Node *updated_node = malloc(sizeof(Common__Node));
        common__node__init(updated_node);
        fill_labels(updated_node, info->changed_labels);
        fill_props(updated_node, info->changed_props);
        request->updated = updated_node;
    }
    request->return_type = info->type_return;
}

int parse_input_cypher(const char *input, Common__Request *pRequest) {
    cypher_parse_result_t *result = cypher_parse(
            input, NULL, NULL, CYPHER_PARSE_ONLY_STATEMENTS);
    if (result == NULL) {
        perror("cypher_parse");
        return EXIT_FAILURE;
    }
    puts("");
    puts("");
    uint8_t errors = cypher_parse_result_nerrors(result);
    if (errors > 0) {
        cypher_parse_result_free(result);
        return -EXIT_FAILURE;
    }
    query_info *info = get_query_info(result);
    if (info == NULL) return EXIT_FAILURE;
    build_client_request(info, pRequest);
    return SOCKET_CODE_OK;
}
